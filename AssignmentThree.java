
public class AssignmentThree {
    public static void main(String[] args) {
        String word="This is a word for testing lamda Method";
        MyClassWithLamda cwl=new MyClassWithLamda();
       System.out.println(cwl.lamdaMethod((str)->str.split(" ").length,word));
    }
}
interface WordCount{
    int count(String word);
}
class MyClassWithLamda
{
    public int lamdaMethod(WordCount wordCount,String word){
        return wordCount.count(word);
    }
}
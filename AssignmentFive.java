
import java.time.*;
import static java.time.temporal.TemporalAdjusters.*;

public class AssignmentFive {
    public static void main(String[] args) {
        LocalDate today= LocalDate.now();
        System.out.println("Today's Date : "+today);
        LocalDate nextMonth=today.plusMonths(1).withDayOfMonth(1);
        System.out.println("Next Month Second Sunday : "+nextMonth.with(next(DayOfWeek.SUNDAY)).with(next(DayOfWeek.SUNDAY)));
    }
}

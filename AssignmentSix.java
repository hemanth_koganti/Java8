import java.time.LocalDate;
import java.time.Period;

public class AssignmentSix {
    public static void main(String[] args) {
        LocalDate startDate=LocalDate.of(2017,8,7);
        LocalDate today=LocalDate.now();
        Period period = Period.between(startDate, today);
        int days = period.getDays();
        int months = period.getMonths();
        int years = period.getYears();
        System.out.println("Years : "+years);
        System.out.println("Months : "+months);
        System.out.println("days : "+days);
    }
}

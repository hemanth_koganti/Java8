
import java.util.ArrayList;

public class AssignmentOne {
    public static void main(String[] args) {

        ArrayList<Employee>list=new ArrayList<>();
        list.add(new Employee(2,"Adam","OnEarth",1000000));
        list.add(new Employee(5,"Thor","Asgard",4000000));
        list.add(new Employee(9,"Tony Stark","OnEarth",9000000));
        list.add(new Employee(8,"Hulk","Planet Hulk",4423400));
        list.add(new Employee(12,"Quill","Xander",4234340));
        list.add(new Employee(6,"Droumamu","DarkDimension",34543000));
        list.add(new Employee(22,"Ego","Planet Ego",3450000));
        list.forEach(System.out::println);
    }
    private static class Employee
    {
        private int id;
        private String name;
        private String address;
        private float salary;

        public Employee(int id, String name, String address, float salary) {
            this.id = id;
            this.name = name;
            this.address = address;
            this.salary = salary;
        }

        public Employee() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }


        public float getSalary() {
            return salary;
        }

        public void setSalary(float salary) {
            this.salary = salary;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        @Override
        public String toString() {
            return "Employee{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", address='" + address + '\'' +
                    ", salary=" + salary +
                    '}';
        }
    }


}
